# Module json-schema
module ValidateJsonSchema
  def validate_json(schema_name, body)
    schema_directory = "#{Dir.pwd}/features/schemas"
    schema_path = "#{schema_directory}/#{schema_name}"
    JSON::Validator.validate!(schema_path, body)
  end
end
  