Feature: List Pokemons

  I as user
  I can list all pokemons

  Scenario: list pokemons with success
  When i make a request
  Then i check the status is sucesss

  Scenario: list pokemon by name
  When i make a request with name 'bulbasaur'
  Then i check the name this pokemon is 'bulbasaur'

  Scenario: list pokemon by id
  When i make a request with id 1
  Then i check the id this pokemon id is 1

  Scenario: list pokemon that does not exist
  When i make a request with pokemon that does not exist
  Then i check the status is 404

  Scenario: Verify endpoint list only twenty pokemons per page
  When i make a request
  Then i check the schema 'pokemons.schema.json' have only twenty pokemons