When("i make a request") do
  @request = PokemonService.get('/pokemon/')
end
  
Then("i check the status is sucesss") do
  expect(@request.code).to eq 200 
end
  
When("i make a request with name {string}") do |name_pokemon|
  @request_name = PokemonService.get("/pokemon/#{name_pokemon}")
end
  
Then("i check the name this pokemon is {string}") do |name|
  expect(@request_name['name']).to eq name
end
  
When("i make a request with id {int}") do |id_pokemon|
  @request_id = PokemonService.get("/pokemon/#{id_pokemon}")
end
  
Then("i check the id this pokemon id is {int}") do |id|
  expect(@request_id['id']).to eq id
end
  
When("i make a request with pokemon that does not exist") do
  @request_not_exist = PokemonService.get("/pokemon/999")
end
  
Then("i check the status is {int}") do |status|
  expect(@request_not_exist.code).to eq status
end
  
Then("i check the schema {string} have only twenty pokemons") do |schema|
  expect(validate_json(schema, @request.parsed_response)).to eq true
end
