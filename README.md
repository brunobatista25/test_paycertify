# Test QA Engineer Paycertify

I did a very simple test,
with the following scenarios:

  Scenario: list pokemons with success

  Scenario: list pokemon by name

  Scenario: list pokemon by id

  Scenario: list pokemon that does not exist

  Scenario: Verify endpoint list only twenty pokemons per page

using Ruby and HttParty for testing api with JsonSchema for contract test,
using gitlab-ci.yml for configure CI in gitlab
and use dockerfile, case you need run test locally in one enviroment safelly.


# Steps for run project

## Pull Project

```
https://gitlab.com/brunobatista25/test_paycertify
```

## Install gems
```
cd test_paycertify
```

```
bundle install
```

# Run test
```
bundle exec cucumber
```


# Plus docker:

## Run test in docker

```
cd test_paycertify
````

## Create a image

This command create image, based in file dockerfile configuration

```
docker build -t <image_name> .
```

## Run image

```
docker run --rm <image_name> bundle exec cucumber
```